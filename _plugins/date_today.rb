# frozen_string_literal: true

module Jekyll
  # Returns the current date in the given format
  class DateToday < Liquid::Tag
    def initialize(tag_name, format, tokens)
      super
      @format = format
    end

    def render(_context)
      Date.today.strftime(@format)
    end
  end
end

Liquid::Template.register_tag('date_today', Jekyll::DateToday)
