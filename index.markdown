---
layout: home
---

<html>
  <head>
    <meta charset="utf-8">
    <title>Home</title>
  </head>
  <body>
    <h1>Hello World</h1>

    <input type="button" name="showYearBtn" value="Show Year" onclick="showBlock('year')" />
    <div id="year" style="display:none;margin-top:10px;">
      {% highlight ruby %}
      {% date_today %Y %}
      {% endhighlight %}
    </div>

    <input type="button" name="showMonthBtn" value="Show Month" onclick="showBlock('month')" />
    <div id="month" style="display:none;margin-top:10px;">
      {% highlight ruby %}
      {% date_today %B %}
      {% endhighlight %}
    </div>

    <input type="button" name="showWeekdayBtn" value="Show Weekday" onclick="showBlock('weekday')" />
    <div id="weekday" style="display:none;margin-top:10px;">
      {% highlight ruby %}
      {% date_today %A %}
      {% endhighlight %}
    </div>
  </body>
</html>

<script>
function showBlock(id) {
   document.getElementById(id).style.display = "block";
}

</script>