# frozen_string_literal: true
require 'spec_helper.rb'

describe 'show date buttons', type: :feature, js: true do
  before do
    visit '/'
  end

  context 'when the "Show Year" button is clicked' do
    it 'shows the correct year' do
      visit '/'
      click_button('Show Year')

      expect(page).to have_content(Date.today.strftime('%Y'))
    end
  end

  context 'when the "Show Month" button is clicked' do
    it 'shows the correct month' do
      visit '/'
      click_button('Show Month')

      expect(page).to have_content(Date.today.strftime('%B'))
    end
  end

  context 'when the "Show Weekday" button is clicked' do
    it 'shows the correct weekday' do
      visit '/'
      click_button('Show Weekday')

      expect(page).to have_content(Date.today.strftime('%A'))
    end
  end
end
