# frozen_string_literal: true

describe 'about page', type: :feature, js: true do
  it 'has expected content' do
    visit 'about'

    expect(page).to have_content('This is the about page')
  end
end
