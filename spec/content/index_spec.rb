# frozen_string_literal: true

describe 'index page', type: :feature, js: true do
  it 'has page title' do
    visit '/'

    expect(page).to have_content('Hello World')
  end
end
